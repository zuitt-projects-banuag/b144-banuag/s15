/*
	Operators
	Add + = sum
	Subtract - = Difference
	Multiply * = Product
	Divide / = Quotient
	Modulus % = Reminder 
*/

function mod(){
	return 9 % 2;
}
console.log(mod());

//Assignment operator (=)
/*
	+= (Addition)
	-= (Subtraction)
	*= (Multipication)
	/= (Division)
	%= (Modulo)

*/
	let x = 1;
	
	let sum = 1;
	// sum = sum + 1;
	sum += 1;
	console.log(sum);
 
/*
	Increment and Decrement
	Operators that add or subtract values by 1 and reassign the value of the variable where the increment/decrement wass applied to.
*/

let z = 1;


//Pre-increment - It will add 1 before returning the value.
let increment = ++z; //Nag add ng 1 value
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// Post Increment
increment = z++; // Nagreassign muna sa increment
console.log("Result of post-increment: " + increment); // bago naging 2
console.log("Result of post-increment: " + z);

//Pre-decrement
let decrement = --z; //nag subtract mo na
console.log("Result of pre-decrement: " + decrement); //bago mag reassign
console.log("Result of pre-decrement: " + z);

//Post-decrement
decrement = z--; 
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

/*Comparison Operators
Kino-compare muna sa value bago maging boolean*/

/*
	Equality Operators (==) 

	1 is equal to true
	0 is equal to false
*/

let juan = 'juan';
console.log(1 == 1);
console.log(0 == false);
console.log('juan' == juan);

/*Strict eqaulity (===)
	yung tinitignan sa strict equality is yung data type
*/
console.log(1 === true);

/* Inequality Operator - nagcompare pag yung 
   certain value are not equal
*/
console.log("Inequality Operator");
console.log(1 != 1);
console.log('Juan' != juan);

/*Strict Inequality (!==)*/
console.log(0 !== false);

/* Other comparison operators 
	> - Greater Than
	< - Less Than
	>= - Greater Than or Equal
	<= - Less Than or Equal
*/

/* Logical Operators
   And Operator (&&) - returns true if all operands are true.
   true && true = true
   false && true = false
   true && false = false
   false && false = false  

   Or Operator (||) - returns true if at least on operands is true 
   true && true = true
   false && true = true
   false && false = false
*/

let isLegalAge = true;
let isRegistered = false;

// And Operator
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND Operator: " + allRequirementsMet);

// Or Operator
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR Operator: " + someRequirementsMet);

// Selection Control Structures
/*
	IF statement 
	- execute a statement if a specified condition is true
	Syntax:
	if(condition){
		statement/s;
	}
*/

let num = -1;
if(num < 0) {
	console.log('Hello');
}

// Mini Activity
let num1 = 10
if(num1 >= 10) {
	console.log("Welcome to Zuitt")
}

/*
	IF-ELSE Statement
	- executes a statement if all the previous condition returns false.
	Syntax:
	if(condition){
		statement/s;
	}
	else{
		statement/s;
	}
*/

num = 5;
if(num >= 10) {
	console.log("Number is greater or equal to 10");
}
else{
	console.log("Number is not greater or equal to 10");
}

// Mini Activity
/*let age = 60;
if (age > 59) {
	console.log("Senior Age");
}
else{
	console.log("Invalid Age");
}*/

/*let age = parseInt(prompt("Please provide age:"));
if (age > 59) {
	alert("Senior Age");
}
else{
	alert("Invalid Age");
}*/

/*	
IF-ELSEIF-ELSE statement - is a multiple condition
	Syntax:
	if(condition){
		statement/s;
	}
	else if(condition){
		statement/s;
	}
	.
	.
	.
	else {
		statement/s;
	}

	1 - Quezon City
	2 - Valuenzuela City
	3 - Pasig City
	4 - Taguig
*/

/*let city = parseInt(prompt("Enter a number: "));

if(city === 1){
	alert("Welcome to Quezon City");
}
else if(city === 2){
	alert("Welcome to Valuenzuela City");
}
else if(city === 3){
	alert("Welcome to Pasig City");
}
else if(city === 4){
	alert("Welcome to Taguig");
}
else{
	alert("Invalid Number");
}
*/

let message = '';

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return 'Not a Typhoon yet';
	}
	else if(windSpeed <= 61){
		return 'Tropical depression detected.';
	}
	else if(windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical strom detected.';
	}
	else if(windSpeed >= 89 && windSpeed <= 117){
		return 'Severe tropical strom detected.';
	}
	else {
		return 'Typhoon detected.';
	}
}

message = determineTyphoonIntensity(70);
console.log(message);

/*Ternary Operator
	One liner
	Syntax:
	 (condition) ? ifTrue : ifFalse

*/
// Example of Ternanry
let ternanryResult = (1 < 18) ? 'valid' : 'invalid';
console.log("Result of ternanry Operator: " + ternanryResult);

// Example
let name;

function isOfLegalAge() {
	name = 'John';
	return 'You are of the legal age limit';
}

function isUnderAge() {
	name = 'Jane';
	return 'You are of under age limit';
}

let age = parseInt(prompt("What is your age?"));
// Tenanry Operator
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge()
alert("Result of ternanry Operator Functions: " + legalAge + ', ' + name);

/*Switch Statement

	Syntax: 
	 switch (expression){
		case value1:
			statement/s;
			break;
		case value2:
			statement/s;
			break;
		case valuen:
			statement/s;	
			break;

		default:
			Statement/s;	
	 }

	.toLowerCase(); It will set it to
	lower case
*/

let day = prompt("What day of the week is it today?").toLowerCase(); 
	// Kung ano yung nasa let ayun ilalagay sa switch
	switch(day){
		case 'sunday':
			alert("The color of the day is red")
			break;
		case 'monday':
			alert("The color of the day is orange")
			break;
		case 'tuesday':
			alert("The color of the day is yellow")
			break;
		case 'wednesday':
			alert("The color of the day is green")
			break;
		case 'thursday':
			alert("The color of the day is blue")
			break;
		case 'friday':
			alert("The color of the day is indigo")
			break;
		case 'saturday':
			alert("The color of the day is violet")
			break;
		
		default:
			alert("Please input valid day");	
	 }

/*
	Try-Catch-Finally Statement - commonly used for error handling
	Try - kung gagana yung line of code
	Catch - if meron error it will handle of catch
	Finally - execute the certain error
*/

function showIntensityAlert(windSpeed){
	try{
		alerta(determineTyphoonIntensity(windSpeed));
	}
	catch(error){
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert('Intersity updates will show you new alert')
	}
}

showIntensityAlert(56);


